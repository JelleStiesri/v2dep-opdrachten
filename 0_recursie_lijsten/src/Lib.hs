module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- | Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.

-- | Ik Schrijf steeds de werking onder de de comment met de opdrachtomschrijving

-- | Steeds item 1 van lijst plus het vorige (niet volgende door de haakjes) item.
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs) = x + (ex1 xs)

-- | Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.

-- | Verhoogt item 1 uit lijst met 1, doet dit daarna met de rest van de lijst opnieuw todat elk item is geweest.
ex2 :: [Int] -> [Int]
ex2 [] = [] --Functie eindigd wanneer de lijst leeg is
ex2 (x:xs) = x + 1 : ex2 xs --x = eerste van lijst, xs= rest van lijst. - Eerste item in lijst + 1, rest opnieuw de functie in todat alles is geweest

-- | Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.

-- | Doet hetzelfde maar dan *-1 ipv +1
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1) : ex3 xs

-- | Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.

-- | Lijst x word stukje voor stukje voor lijst Y geplakt. Todat X leeg is.
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] z = z
ex4 (x : xs) y = x : ex4 xs y

-- | Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.

-- | Lijkt op functie 1, doet eerste item 1 uit de 2 lijsten met elkaar, daarna 2, ect.
ex5 :: [Int] -> [Int] -> [Int]
ex5 z [] = z
ex5 (x:xs) (y:ys) = x + y : ex5 xs ys

-- | Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.

-- | Hetzelfde maar dan * elkaar.
ex6 :: [Int] -> [Int] -> [Int]
ex6 z [] = z
ex6 (x:xs) (y:ys) = x * y : ex6 xs ys

-- | Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.

<<<<<<< HEAD
-- | Gebruikt eerst ex6 om te vermenigvuldigen en dan ex1 om de som hiervan te berekenen.
=======
-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig product uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
>>>>>>> 15f6e502c952be37312eddb3823ebe24ca7bc074
ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1 (ex6 x y) --Berekend eerst de 2 lijsten vermenigvuldigt en berekend hier vervolgens de som uit.
